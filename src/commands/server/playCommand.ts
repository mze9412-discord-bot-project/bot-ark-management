import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CacheType, Util } from "discord.js";
import { ArkServerDatabaseProvider } from "../../database/arkServerDatabaseProvider";

export class PlayCommand extends BotCommand {
    constructor(private _arkServerDbProvider: ArkServerDatabaseProvider) {
        super('play', 'List servers including steam://connect links.', 'EVERYONE');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const cmdBuilder = new SlashCommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            ;

        return cmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const commandName = interaction.commandName;
        return commandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction<CacheType>): Promise<void> {
        if (interaction.guild == null) return;

        const servers = await this._arkServerDbProvider.getAll(interaction.guild.id);

        if(servers.length === 0) {
            await interaction.reply('There are no servers. Sorry!');
            return;
        }
        
        const serverEntries: string[] = [];
        serverEntries.push(`Hey ${interaction.user.username}! There are ${servers.length} servers to play on! Here's a list for you:`);
        for(let i = 0; i < servers.length; i++) {
            const server = servers[i];
            serverEntries.push(`${(i+1)}) ${server.friendlyName} - steam://connect/${server.externalIp}:${server.queryPort}`);
        }
        serverEntries.push('\nHave fun playing on our servers!');

        const split = Util.splitMessage(serverEntries.join('\n'));
        await interaction.reply(split[0]);
        for (let i = 1; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }
}