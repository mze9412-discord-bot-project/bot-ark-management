import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CommandInteractionOption, Util } from "discord.js";
import { ArkServer } from "../..";
import { ArkServerDatabaseProvider } from "../../database/arkServerDatabaseProvider";

export class ServerCommand extends BotCommand {
    constructor(private _arkServerDbProvider: ArkServerDatabaseProvider) {
        super('server', 'Manage your list of registered servers.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const groupCmdBuilder = new SlashCommandSubcommandGroupBuilder()
            .setName(this.name)
            .setDescription(this.description);

        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('add')
                .setDescription('Add a new server to your registered Ark servers.')
                .addStringOption(o =>
                    o.setName('name')
                        .setDescription('Internal name of your server.')
                        .setRequired(true)
                )
                .addStringOption(o =>
                    o.setName('friendlyname')
                        .setDescription('Frindly name of your server, displayed when presented to users.')
                        .setRequired(true)
                )
                .addStringOption(o =>
                    o.setName('externalip')
                        .setDescription('IP or hostname of your server to which players connect.')
                        .setRequired(true)
                )
                .addStringOption(o =>
                    o.setName('internalip')
                        .setDescription('Internal IP or hostname of your server to use with tools like rcon, can be same as external IP.')
                        .setRequired(true)
                )
                .addIntegerOption(o =>
                    o.setName('queryport')
                        .setDescription('Query port (for steam favories and steam://connect links) of your server.')
                        .setRequired(true)
                )
                .addIntegerOption(o =>
                    o.setName('rconport')
                        .setDescription('RCON port of your server.')
                        .setRequired(true)
                )
                .addStringOption(o =>
                    o.setName('rconpassword')
                        .setDescription('Password to execute RCON commands on the server.')
                        .setRequired(true)
                )
            );
            
        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('delete')
                .setDescription('Delete one of your registered Ark servers.')
                .addStringOption(o =>
                    o.setName('name')
                        .setDescription('Name of your server.')
                        .setRequired(true)
                )
            );
        
        groupCmdBuilder.addSubcommand(cmd =>
            cmd.setName('list')
                .setDescription('List your registered Ark servers.')
            );

        return groupCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction): Promise<void> {
        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];

        if (subCommand.name === 'list') {
            await this.handleServerList(interaction);   
        } else if (subCommand.name === 'add') {
            await this.handleServerAdd(interaction);
        } else if (subCommand.name === 'delete') {
            await this.handleServerDelete(interaction);
        }
    }

    private async handleServerList(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const servers = await this._arkServerDbProvider.getAll(interaction.guild.id);

        if(servers.length === 0) {
            await interaction.reply('There are no servers. Sorry! Maybe you want to add some with the *server add* command?');
            return;
        }
        
        const serverEntries: string[] = [];
        serverEntries.push(`There are ${servers.length} servers. Here's a list for you:`);
        for(let i = 0; i < servers.length; i++) {
            const server = servers[i];
            serverEntries.push(`${(i+1)}) ${server.name} (friendly: ${server.friendlyName}) @ ${server.externalIp}:${server.queryPort} (RCON: ${server.internalIp}:${server.rconPort}).`);
        }
        
        const split = Util.splitMessage(serverEntries.join('\n'));
        await interaction.reply(split[0]);
        for (let i = 1; i < split.length; i++) {
            await interaction.followUp(split[i]);
        }
    }

    private async handleServerAdd(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        const server = new ArkServer();
        server.guildId = interaction.guild.id;
        for (const o of parameters) {
            if (o.name === 'name') server.name = <string>o.value;
            if (o.name === 'friendlyname') server.friendlyName = <string>o.value;
            if (o.name === 'externalIP') server.externalIp = <string>o.value;
            if (o.name === 'internalIp') server.internalIp = <string>o.value;
            if (o.name === 'queryport') server.queryPort = <number>o.value;
            if (o.name === 'rconport') server.rconPort = <number>o.value;
            if (o.name === 'rconpassword') server.rconPassword = <string>o.value;
        }
        
        const success = await this._arkServerDbProvider.store(server, false);
        if (success) {
            await interaction.reply(`The server *${server.name}* was successfully added.`);
        } else {
            await interaction.reply(`There is already a server with the name *${server.name}*.`);
        }
    }

    private async handleServerDelete(interaction: BaseCommandInteraction): Promise<void> {
        if (interaction.guild == null) return;

        const opts = interaction.options;
        const subCommandGroup = <CommandInteractionOption>opts.data[0];
        const subCommand = (<CommandInteractionOption[]>subCommandGroup.options)[0];
        const parameters = <CommandInteractionOption[]>subCommand.options;

        const name = <string>parameters[0].value;
        
        // get server from DB
        const server = await this._arkServerDbProvider.get(interaction.guild.id, name);
        if (server == null) {
            await interaction.reply(`There is no server with the name *${name}*.`);
            return;
        }

        // delete server
        const success = await this._arkServerDbProvider.delete(server) === 1;
        if (success) {
            await interaction.reply(`The server *${name}* was successfully deleted.`);
        } else {
            await interaction.reply(`Failed to deleted server *${name}*.`);
        }
    }
}