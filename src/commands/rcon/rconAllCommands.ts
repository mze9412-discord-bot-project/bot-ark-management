import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CacheType, CommandInteractionOption, CommandInteractionOptionResolver, Util } from "discord.js";
import { ArkServerDatabaseProvider } from "../../database/arkServerDatabaseProvider";
import { RconService } from "../../services/rcon.service";

export class RconAllCommand extends BotCommand {
    constructor(private _rconService: RconService, private _arkServerDbProvider: ArkServerDatabaseProvider) {
        super('rconall', 'Send an rcon command to all your registered Ark servers.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const subCmdBuilder = new SlashCommandSubcommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(opt => 
                opt.setName('command')
                    .setDescription('RCON command with all parameters.')
                    .setRequired(true)
            )
            ;

        return subCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction<CacheType>, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction<CacheType>): Promise<void> {
        if (interaction.guild == null) return;

        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }

        await interaction.reply({ content: 'Sending command. Please stand by ...', ephemeral: true });

        let command = '';

        // read options
        const opts = <CommandInteractionOptionResolver>interaction.options;
        if (opts.getSubcommand() === this.name) {
            const subCommand = <CommandInteractionOption[]>opts.data;
            const options = <CommandInteractionOption[]>subCommand[0].options;

            for (const o of options) {
                if (o.name === 'command') command = <string>o.value;
            }
        }

        // check content
        if (command === '') {
            await interaction.reply('Sorry, empty command is not possible.');
        }
        
        const guildId = interaction.guild.id;
        const servers = await this._arkServerDbProvider.getAll(guildId);
        if(servers.length === 0) {
            await interaction.followUp('No servers found to execute the command on. Maybe add some with command *addserver* first.');
        } else {
            // create result buffer
            const buffer: string[] = [];
            buffer.push(`Command *"${command}"* executed on *${servers.length}* servers.`);

            // execute command for each server
            for(const server of servers) {
                const result = await this._rconService.runRconCommand(server, command);
                buffer.push(`**${server.name}**`);
                buffer.push(result);
                buffer.push('\n');
            }

            const split = Util.splitMessage(buffer.join('\n'));
            for (const part of split) {
                await interaction.followUp(part);
            }
        }
    }

}