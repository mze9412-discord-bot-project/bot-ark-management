import { SlashCommandSubcommandBuilder, SlashCommandSubcommandGroupBuilder, SlashCommandBuilder } from "@discordjs/builders";
import { BotCommand } from "@mze9412-discord-bot-project/bot-runner";
import { BaseCommandInteraction, CacheType, CommandInteractionOption, CommandInteractionOptionResolver, Util } from "discord.js";
import { ArkServerDatabaseProvider } from "../../database/arkServerDatabaseProvider";
import { RconService } from "../../services/rcon.service";

export class RconCommand extends BotCommand {
    constructor(private _rconService: RconService, private _arkServerDbProvider: ArkServerDatabaseProvider) {
        super('rcon', 'Send an rcon command to one of your registered Ark servers.', 'ADMINISTRATOR');
    }

    buildCommand(): SlashCommandSubcommandBuilder | SlashCommandSubcommandGroupBuilder | SlashCommandBuilder {
        const subCmdBuilder = new SlashCommandSubcommandBuilder()
            .setName(this.name)
            .setDescription(this.description)
            .addStringOption(opt => 
                opt.setName('servername')
                    .setDescription('Name of your server')
                    .setRequired(true)
            )
            .addStringOption(opt => 
                opt.setName('command')
                    .setDescription('RCON command with all parameters.')
                    .setRequired(true)
            )
            ;

        return subCmdBuilder;
    }

    async isMatching(interaction: BaseCommandInteraction<CacheType>, moduleName: string): Promise<boolean> {
        const opts = interaction.options;
        const commandName = interaction.commandName;
        const subCommandName = opts.data.length > 0 ? opts.data[0].name : '';
        return commandName === moduleName && subCommandName === this.name;
    }

    async execute(interaction: BaseCommandInteraction<CacheType>): Promise<void> {
        if (interaction.guild == null) return;

        if (!interaction.memberPermissions?.has("ADMINISTRATOR")) {
            await interaction.reply({ content: 'Sorry, you are not allowed to use this command.', ephemeral: true });
            return;
        }

        await interaction.reply({ content: 'Sending command. Please stand by ...', ephemeral: true });

        let servername = '';
        let command = '';

        // read options
        const opts = <CommandInteractionOptionResolver>interaction.options;
        if (opts.getSubcommand() === this.name) {
            const subCommand = <CommandInteractionOption[]>opts.data;
            const options = <CommandInteractionOption[]>subCommand[0].options;

            for (const o of options) {
                if (o.name === 'servername') servername = <string>o.value;
                if (o.name === 'command') command = <string>o.value;
            }
        }

        // empty name not possible!
        if (servername === '') {
            await interaction.followUp('Sorry, empty servername is not possible.');
            return;
        }
        if (command === '') {
            await interaction.followUp('Sorry, empty command is not possible.');
            return;
        }
        
        const guildId = interaction.guild.id;
        const server = await this._arkServerDbProvider.get(guildId, servername);
        if(server == null) {
            await interaction.reply(`No registered server named *${servername}* exists.`);
        } else {
            // create result buffer
            const buffer: string[] = [];
            buffer.push(`Command *${command}* executed on server *${servername}*.`);

            // execute command
            const result = await this._rconService.runRconCommand(server, command);
            buffer.push(result);

            const split = Util.splitMessage(buffer.join('\n'));
            for (const part of split) {
                await interaction.followUp(part);
            }
        }
    }
}