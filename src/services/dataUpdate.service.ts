// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Ark-Management <https://gitlab.com/mze9412-discord-bot-project/bot-ark-management>.
//
// Bot-Ark-Management is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Ark-Management is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Ark-Management. If not, see <http://www.gnu.org/licenses/>.

import { BotLogger, Colors, DiscordChannelService, DiscordMessageService, sleep, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { DiscordSlugService, TYPES_BOTSHARED } from "@mze9412-discord-bot-project/bot-shared";
import { MessageEmbed, TextChannel } from "discord.js";
import { inject, injectable } from "inversify";
import { Logger } from "tslog";
import { ArkServerDatabaseProvider } from "../database/arkServerDatabaseProvider";
import { ArkServerManagementConfigDatabaseProvider } from "../database/arkServerManagementConfigDatabaseProvider";
import { ArkServer } from "../models/arkServer";
import { ArkServerManagementConfig } from "../models/arkServerManagementConfig";
import { TYPES_ARKMANAGEMENT } from "../TYPES";
import { RconService } from "./rcon.service";
import { ServerQueryService } from "./serverQuery.service";

@injectable()
export class DataUpdateService {
    private _logger!: Logger;

    constructor(
        @inject(TYPES_BOTSHARED.DiscordSlugService) private _discordSlugService : DiscordSlugService,
        @inject(TYPES_ARKMANAGEMENT.ArkServerDatabaseProvider) private _arkServerProvider : ArkServerDatabaseProvider,
        @inject(TYPES_ARKMANAGEMENT.ArkServerManagementConfigDatabaseProvider) private _arkServerManagementConfigProvider : ArkServerManagementConfigDatabaseProvider,
        @inject(TYPES_BOTRUNNER.DiscordChannelService) private _discordChannelService : DiscordChannelService,
        @inject(TYPES_BOTRUNNER.DiscordMessageService) private _discordMessageService : DiscordMessageService,
        @inject(TYPES_ARKMANAGEMENT.RconService) private _rconService : RconService,
        @inject(TYPES_ARKMANAGEMENT.ServerQueryService) private _queryService : ServerQueryService,
        @inject(TYPES_BOTRUNNER.BotLogger) private _botLogger: BotLogger
    ) {
        this._logger = _botLogger.getChildLogger('DataUpdateService');
    }

    public async performDataUpdate() : Promise<void> {
        const configs = await this._arkServerManagementConfigProvider.getAll();
        
        for (const config of configs) {
            // skip if not enabled
            if (!config.playerListEnabled && !config.statusChannelsEnabled) {
                continue;
            }

            // get servers
            const servers = await this._arkServerProvider.getAll(config.guildId);
            
            // run specific updates
            if (config.playerListEnabled) await this.performPlayerListUpdate(config, servers);
            if (config.statusChannelsEnabled) await this.performStatusChannelsUpdate(config, servers);
        }
    }

    private async performPlayerListUpdate(config: ArkServerManagementConfig, servers: ArkServer[]): Promise<void> {
        const playerListCategoryIsStatusChannelCategory = config.playerListCategory === config.statusChannelsCategory;
        const categorySlugName = playerListCategoryIsStatusChannelCategory ? 'statusCategory' : 'playerListCategory';

        // get category and channel
        const category = await this._discordSlugService.getCategoryBySlug(config.guildId, categorySlugName, config.playerListCategory, true, config.playerListPublic);
        if(category == undefined) {
            return; // TODO
        }
        const channel = await this._discordSlugService.getTextChannelBySlug(config.guildId, 'playerListChannel', config.playerListChannel, category, true, config.playerListPublic);
        if(channel == undefined) {
            return; // TODO
        }

        // total count for summary post
        let countPlayersTotal = 0;
        let countOnlineServers = 0;

        // get player lists and post to discord
        for (const server of servers) {
            const info = await this._queryService.queryServerDirect(server);

            // format message content
            let statusString = '';
            let color = Colors.blue;
            if (info.online) {
                countOnlineServers++;
                countPlayersTotal += info.playerCount;
                statusString = `${info.playerCount === 0 ? '💙' : '💚'}${info.name} - ${info.playerCount}/${info.maxPlayers}`
                if (info.playerCount > 0) color = Colors.green;
            } else {
                color = Colors.red;
                statusString = `💔${info.name}`;
            }

            // create players string
            let players = info.players.join('\n');
            if (players.length === 0) {
                players = '-\n\u200b';
            } else {
                players = players + '\n\u200b';
            }

            // if player text is too long, try again without steam IDs
            if (players.length > 1024) { // 1024 is the limit for field values according to https://discordjs.guide/popular-topics/embeds.html#resending-a-received-embed
                players = info.playersNoSteam.join('\n') + '\n\u200b';
            }
            
            // create specific embed message
            const listMsg = new MessageEmbed()
            .setAuthor(statusString, 'https://gitlab.com/mze9412/arkrconbot/-/raw/main/ArkRconBot.png', 'https://gitlab.com/mze9412/arkrconbot') // TODO
            .setTimestamp()
            .setFooter('Brought to you by ArkRconBot - RCON for the masses!') // TODO
            .addField('Players', players)
            .setColor(color);

            // get post for server
            const message = await this._discordSlugService.getMessageBySlug(config.guildId, 'playerlist_' + server.name, channel);
            if (message == undefined) {
                // create new message
                const msgId = await this._discordMessageService.sendEmbedMessage(<TextChannel>channel, listMsg);
                await this._discordSlugService.createMessageSlugById(config.guildId, 'playerlist_' + server.name, msgId);
            } else {
                await message?.edit({ embeds: [ listMsg ]});
            }

            await sleep(2000);
        }

        // select overview color
        let color = Colors.blue;
        if (countOnlineServers === 0) {
            color = Colors.red;
        } else if (countOnlineServers === servers.length) {
            color = Colors.green;
        }

        // create general message and specific messages for each server
        const overViewListMsg = new MessageEmbed()
            .setAuthor('Overview', 'https://gitlab.com/mze9412/arkrconbot/-/raw/main/ArkRconBot.png', 'https://gitlab.com/mze9412/arkrconbot')  // TODO
            .setTimestamp()
            .setFooter('Brought to you by ArkRconBot - RCON for the masses!')  // TODO
            .addField('Players Total', `${countPlayersTotal}`)
            .addField('Servers Online', `${countOnlineServers}/${servers.length}`)
            .setColor(color);
    
            const message = await this._discordSlugService.getMessageBySlug(config.guildId, 'playerlist_overview_message', channel);
        if (message == undefined) {
            // create new message
            const msgId = await this._discordMessageService.sendEmbedMessage(<TextChannel>channel, overViewListMsg);
            await this._discordSlugService.createMessageSlugById(config.guildId, 'playerlist_overview_message', msgId);
        } else {
            await message?.edit({embeds: [ overViewListMsg ]});
        }
        
        await sleep(2000);
    }

    private async performStatusChannelsUpdate(config: ArkServerManagementConfig, servers: ArkServer[]): Promise<void> {
        const playerListCategoryIsStatusChannelCategory = config.playerListCategory === config.statusChannelsCategory;
        const categorySlugName = playerListCategoryIsStatusChannelCategory ? 'statusCategory' : 'statusChannelsCategory';

        const category = await this._discordSlugService.getCategoryBySlug(config.guildId, categorySlugName, config.statusChannelsCategory, true, config.statusChannelsUsable);
        if(category == undefined) {
            this._logger.error('Could not find category for Status Channels Updates');
            return;
        }
        
        for (const server of servers) {
            const info = await this._queryService.queryServerDirect(server);

            // configure channel name
            // decide string
            let statusString = '';
            if (info.online) {
                statusString = `${info.playerCount === 0 ? '💙' : '💚'}${info.name} - ${info.playerCount}/${info.maxPlayers}`
            } else {
                statusString = `💔${info.name}`;
            }
            this._logger.debug(`Status: ${statusString}`);

            // get channel        
            const channel = await this._discordSlugService.getVoiceChannelBySlug(config.guildId, 'statusChannel_' + server.name, server.name, category, true, config.playerListPublic);
            if(channel == undefined) {
                this._logger.error(`Could not find channel for server ${server.name}`);
                continue;
            }
            await channel?.setName(statusString);
        }
    }

    public async performChatLogUpdate(): Promise<void> {
        const configs = await this._arkServerManagementConfigProvider.getAll();
        this._logger.debug(`Chat Logging configs found: ${configs.length}`);
        
        for (const config of configs) {
            // skip if not enabled
            if (!config.chatLoggingEnabled) {
                this._logger.debug(`Chat Logging is not enabled for ${config.guildId}`);
                continue;
            }

            // get servers
            const servers = await this._arkServerProvider.getAll(config.guildId);
            
            // run specific updates
            await this.performChatLogUpdateForGuild(config, servers);
        }
    }

    private async performChatLogUpdateForGuild(config: ArkServerManagementConfig, servers: ArkServer[]): Promise<void> {
        this._logger.info(`Chat Logging update for ${config.guildId}`);

        // get category
        const category = await this._discordSlugService.getCategoryBySlug(config.guildId, 'chatLoggingCategory', config.chatLoggingCategory, true, config.chatLoggingPublic);
        if(category == undefined) {
            this._logger.warn(`Chat Logging category could not be found for ${config.guildId}. Category: ${config.chatLoggingCategory}`);
            return;
        }

        // create buffers
        const adminLogBuffer: string[] = [];
        const tribeLogBuffer: string[] = [];
        
        let numberOfMessages = 0; // used for throttling
        for (const server of servers) {
            const chatString = await this._rconService.runRconCommand(server, 'GetChat');

            // divide at \n (newline) and parse
            const chatLines = chatString.split('\n');

            // skip to next server if no chat
            if (chatLines.length === 0) continue;
            
            // define channelName
            const channelName = server.name.replace(' ', '_').toLowerCase();
        
            // get or create server log channel
            const serverLogChannel = await this._discordSlugService.getTextChannelBySlug(config.guildId, 'chatLog_'  + channelName, channelName, category, true, config.chatLoggingPublic);
            if(serverLogChannel == undefined) {
                this._logger.warn(`Server log channel not found for ${config.guildId}. Channel: channelName`);
                continue; // TODO
            }

            // parse each line and format for posting to discord channel
            const buffer: string[] = [];
            for (const rawLine of chatLines) {
                const line = await this.parseAndFormatChatLine(server.name, rawLine);
                // skip empty lines
                if (line === '') continue;
            
                if (line.startsWith('**ADMIN**')) {
                    adminLogBuffer.push(line);
                } else if (line.startsWith('**TRIBE**')) {
                    tribeLogBuffer.push(line);
                } else {
                    buffer.push(line);
                }
            }

            // send message
            if (buffer.length > 0) await this._discordMessageService.sendTextMessage(serverLogChannel, buffer.join('\n'));

            // sleep 5 seconds if we already sent four messages in a row
            numberOfMessages++;
            if (numberOfMessages % 4 === 0) {
                sleep(5000);
            }
        }

        // get channels
        const adminLogChannel = await this._discordSlugService.getTextChannelBySlug(config.guildId, 'adminLogChannel', 'adminLog', category, true, config.chatLoggingPublic);
        const tribeLogChannel = await this._discordSlugService.getTextChannelBySlug(config.guildId, 'tribeLogChannel', 'tribeLog', category, true, config.chatLoggingPublic);
        if(adminLogChannel == undefined || tribeLogChannel == undefined) {
            this._logger.warn(`Chat Logging channels could not be found for ${config.guildId}.`);
            return; // TODO
        }

        // send admin and tribe logs
        if (adminLogBuffer.length > 0) await this._discordMessageService.sendTextMessage(adminLogChannel, adminLogBuffer.join('\n'));
        if (tribeLogBuffer.length > 0) await this._discordMessageService.sendTextMessage(tribeLogChannel, tribeLogBuffer.join('\n'));
        
        this._logger.info(`Chat Logging update finished  for ${config.guildId}`);
    }

    private async parseAndFormatChatLine(mapName: string, rawLine: string): Promise<string> {
        // excludes lines starting with the following
        const chatExcludeLines = [
            'Server received, But no response!!',
            'An error occured during execution of the RCON command on server'
        ];
        for (const exclude of chatExcludeLines) {
            // if line is to be excluded, return empty string
            if (rawLine.startsWith(exclude)) {
                return '';
            }
        }

        // trim
        rawLine = rawLine.trim();
        if (rawLine === '') {
            return '';
        }

        // deal with types of messages
        if (rawLine.startsWith('AdminCmd: ')) {
            return `**ADMIN** ${rawLine.substring(10)}`;
        } else if (rawLine.startsWith('Tribe ')) {
            // remove Tribe heading
            const line = rawLine.substring(6);
            
            // remove RichColor parts and replace with * for cursive
            const regex = /<RichColor Color="(.*)">(.*)<\/>/;
            const result = regex.exec(line);
            if (result != undefined && result.length === 3) {                
                const part1 = line.substring(0, line.indexOf('<Rich'));
                const part2 = result[2];
                return `**TRIBE** (${mapName}) ${part1}${part2}`;
            }

            // done
            return `**TRIBE** ${line}`;
        } else {
            // this is regular chat
            const nameIndex = rawLine.indexOf(':');
            const name = rawLine.substring(0, nameIndex);
            const message = rawLine.substring(nameIndex+1);

            return `**${name}:** ${message.trim()}`;
        }
    }
}