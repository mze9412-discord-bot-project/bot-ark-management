// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Ark-Management <https://gitlab.com/mze9412-discord-bot-project/bot-ark-management>.
//
// Bot-Ark-Management is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Ark-Management is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Ark-Management. If not, see <http://www.gnu.org/licenses/>.

import { ArkServer } from "../models/arkServer";
import { inject, injectable } from "inversify";
import axios from 'axios';
import { TYPES_ARKMANAGEMENT } from "../TYPES";
import { RconService } from "./rcon.service";
import { ArkServerInfo } from "../models/arkServerInfo";
import { query as gamedig } from 'gamedig';

@injectable()
export class ServerQueryService {
    constructor(
        @inject(TYPES_ARKMANAGEMENT.RconService) private _rconService: RconService
    ) { }

    public async queryServer(server: ArkServer): Promise<ArkServerInfo> {
        try {
            //read data 
            const url = `https://api.7nz.de/ark/json/server-info/${server.externalIp}:${server.queryPort}`;
            const result = await axios.get(url);

            let jsonData = undefined;
            if (result.status === 200) {
                jsonData = result.data;
            }
            // use JSON data
            const info = new ArkServerInfo();
            info.server = server; // store server data
            info.online = jsonData.online;
            if (info.online) {
                info.name = server.friendlyName;
                info.maxPlayers = jsonData.max_players;

                if (jsonData.players > 0) {
                    const list = await this._rconService.listPlayers(server);
                    info.players = list[0];
                    info.playersNoSteam = list[1];
                    info.playerCount = info.players.length;
                }
            } else {
                info.name = server.friendlyName + ' - Offline';
                info.maxPlayers = -1;
                info.playerCount = -1;
            }

            // add to result list
            return info;
        } catch (ex) {
            const info = new ArkServerInfo();
            info.name = server.friendlyName + '- Exception';
            info.maxPlayers = -1;
            info.maxPlayers = -1;
            return info;
        }
    }

    public async queryServerDirect(server: ArkServer): Promise<ArkServerInfo> {
        const info = new ArkServerInfo();
        info.server = server; // store server data

        try {
            const result = await gamedig({
                type: 'arkse',
                host: server.externalIp,
                port: server.queryPort,
                attemptTimeout: 5000,
                maxAttempts: 3
            });

            info.name = server.friendlyName;
            info.online = true;

            if (result.players.length > 0) {
                const list = await this._rconService.listPlayers(server);
                info.players = list[0];
                info.playersNoSteam = list[1];
                info.playerCount = info.players.length;
            }
            info.maxPlayers = result.maxplayers;
        } catch (ex) {  
            info.name = server.friendlyName + ' - Offline';
            info.maxPlayers = -1;
            info.playerCount = -1;
        }
        return info;
    }
}