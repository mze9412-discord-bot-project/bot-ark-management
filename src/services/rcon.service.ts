// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Ark-Management <https://gitlab.com/mze9412-discord-bot-project/bot-ark-management>.
//
// Bot-Ark-Management is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Ark-Management is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Ark-Management. If not, see <http://www.gnu.org/licenses/>.

import { ArkServer } from "../models/arkServer";
import { Rcon } from "rcon-client"
import { injectable } from "inversify";

@injectable()
export class RconService {
    public async runRconCommand(server: ArkServer, command: string): Promise<string> {
        let result = '';
        try {
            const rcon = await Rcon.connect({ host: server.internalIp, port: server.rconPort, password: server.rconPassword });
            result = await rcon.send(command);
            rcon.end();
        } catch(e) {
            result = `An error occured during execution of the RCON command on server *${server.name}*.`;
        }
        return result;
    }
    
    /**
     * Helper method to execute list players command via rcon
     * @param server ark server
     * @returns list of players as returned by rcon
     */
    public async listPlayers(server: ArkServer): Promise<[string[], string[]]> {
        const result = await this.runRconCommand(server, 'listplayers');
        
        // players result list
        const players: string[] = [];
        const playersNoSteam: string[] = [];

        let playerNumber = 1;
        for (let entry of result.split('\n')) {
            // trim entry, empty entries are skipped
            entry = entry.trim();
            if (entry.length === 0) {
                continue;
            }

            // test regex and add formatted player data
            const regex = /^\d*.\s(.*),\s(\d*)/;
            const regexResult = regex.exec(entry);
            if (regexResult != null && regexResult.length === 3) {
                players.push(`${playerNumber.toFixed(0)}) __${regexResult[1]}__ (${regexResult[2]})`);
                playersNoSteam.push(`${playerNumber.toFixed(0)}) __${regexResult[1]}__`);
            }
            playerNumber++;
        }

        return [players, playersNoSteam];
    }
}