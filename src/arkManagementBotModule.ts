// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Ark-Management <https://gitlab.com/mze9412-discord-bot-project/bot-ark-management>.
//
// Bot-Ark-Management is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Ark-Management is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Ark-Management. If not, see <http://www.gnu.org/licenses/>.

import { SlashCommandBuilder } from "@discordjs/builders";
import { BotLogger, BotModule, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { Mutex } from "async-mutex";
import { BaseCommandInteraction, CacheType, CommandInteractionOption, CommandInteractionOptionResolver } from "discord.js";
import { inject, injectable } from "inversify";
import { schedule, ScheduledTask } from "node-cron";
import { ArkServerManagementConfigDatabaseProvider } from "./database/arkServerManagementConfigDatabaseProvider";
import { ArkServerDatabaseProvider } from "./database/arkServerDatabaseProvider";
import { DataUpdateService } from "./services/dataUpdate.service";
import { RconService } from "./services/rcon.service";
import { TYPES_ARKMANAGEMENT } from "./TYPES";
import { ArkServerManagementConfig } from "./models/arkServerManagementConfig";
import { RconAllCommand } from "./commands/rcon/rconAllCommands";
import { RconCommand } from "./commands/rcon/rconCommand";
import { ServerCommand } from "./commands/server/serverCommand";
import { PlayCommand } from "./commands/server/playCommand";

@injectable()
export class ArkManagementBotModule extends BotModule {
    private _updateTask: ScheduledTask | undefined = undefined;
    private _updateMutex = new Mutex();
    private _chatLogTask: ScheduledTask | undefined = undefined;
    private _chatLogMutex = new Mutex();

    constructor(
        @inject(TYPES_ARKMANAGEMENT.ArkServerDatabaseProvider) private _arkServerDbProvider: ArkServerDatabaseProvider,
        @inject(TYPES_ARKMANAGEMENT.RconService) private _rconService: RconService,
        @inject(TYPES_ARKMANAGEMENT.DataUpdateService) private _dataUpdateService: DataUpdateService,
        @inject(TYPES_ARKMANAGEMENT.ArkServerManagementConfigDatabaseProvider) private _arkServerManageConfigProvider: ArkServerManagementConfigDatabaseProvider,
        @inject(TYPES_BOTRUNNER.BotLogger) private _botLogger: BotLogger
    ) {
        super('arkmanagement', 'Ark Server management module with RCON functionality.', _botLogger)
    }

    async initializeCore(): Promise<void> {
        this.addSubCommand(new RconAllCommand(this._rconService, this._arkServerDbProvider));
        this.addSubCommand(new RconCommand(this._rconService, this._arkServerDbProvider));
        this.addSubCommand(new ServerCommand(this._arkServerDbProvider));
        this.addSubCommand(new PlayCommand(this._arkServerDbProvider));
        // TODO add commands
    }
    
    async buildConfigCommand(configCommandBuilder: SlashCommandBuilder): Promise<void> {
        configCommandBuilder.addSubcommand(scmd =>
            scmd.setName(this.name)
            .setDescription('Module specific configuration')

            // Status Channels
            .addBooleanOption(opt =>
                opt.setName('statuschannelsenabled')
                .setDescription('Do you want to enable server status display via voice channels?')
                .setRequired(true)
            )
            .addBooleanOption(opt =>
                opt.setName('statuschannelsusable')
                .setDescription('Should status channels be usable for players as voice channels?')
                .setRequired(true)
            )
            .addStringOption(opt =>
                opt.setName('statuschannelscategory')
                .setDescription('What is the category name for status channels? Recommended: STATUS')
                .setRequired(true)
            )
            
            // Player Listing
            .addBooleanOption(opt =>
                opt.setName('playerlistenabled')
                .setDescription('Do you want to enable player listing?')
                .setRequired(true)
            )
            .addBooleanOption(opt =>
                opt.setName('playerlistpublic')
                .setDescription('Do you want to publically show the player listing?')
                .setRequired(true)
            )
            .addStringOption(opt =>
                opt.setName('playerlistcategory')
                .setDescription('What is the category name for player listing? Recommended: STATUS')
                .setRequired(true)
            )
            .addStringOption(opt =>
                opt.setName('playerlistchannel')
                .setDescription('What is the channel name for player listing? Recommended: players')
                .setRequired(true)
            )
            
            // Chat logging
            .addBooleanOption(opt =>
                opt.setName('chatloggingenabled')
                .setDescription('Do you want to enable ingame chat logging?')
                .setRequired(true)
            )
            .addBooleanOption(opt =>
                opt.setName('chatloggingpublic')
                .setDescription('Do you want to publically show the chat logs?')
                .setRequired(true)
            )
            .addStringOption(opt =>
                opt.setName('chatloggingcategory')
                .setDescription('What is the category name for chat logging? Recommended: CHATLOG')
                .setRequired(true)
            )
        );
    }
    
    async handleConfigCommand(interaction: BaseCommandInteraction<CacheType>): Promise<void> {
        this.logger.info('Handling Config Command.');
        if (interaction.guild == null) return;

        const opts = <CommandInteractionOptionResolver>interaction.options;
        const subCommand = <CommandInteractionOption[]>opts.data;
        const options = <CommandInteractionOption[]>subCommand[0].options;

        const vals = new ArkServerManagementConfig();
        vals.guildId = interaction.guild.id;
        
        for(const o of options) {
            if (o.name === 'statuschannelsenabled') vals.statusChannelsEnabled = <boolean>o.value;
            if (o.name === 'statuschannelusable') vals.statusChannelsUsable = <boolean>o.value;
            if (o.name === 'statuschannelscategory') vals.statusChannelsCategory = <string>o.value;
            
            if (o.name === 'playerlistenabled') vals.playerListEnabled = <boolean>o.value;
            if (o.name === 'playerlistusable') vals.playerListPublic = <boolean>o.value;
            if (o.name === 'playerlistcategory') vals.playerListCategory = <string>o.value;
            if (o.name === 'playerlistchannel') vals.playerListChannel = <string>o.value;

            if (o.name === 'chatloggingenabled') vals.chatLoggingEnabled = <boolean>o.value;
            if (o.name === 'chatloggingpublic') vals.chatLoggingPublic = <boolean>o.value;
            if (o.name === 'chatloggingcategory') vals.chatLoggingCategory = <string>o.value;

            this.logger.debug(`Configured option: ${o.name}. Value: ${o.value}`);
        }
        

        this.logger.debug('Saving ArkServerManagementConfig');
        const result = await this._arkServerManageConfigProvider.store(vals, true);
        this.logger.debug('Saving ArkServerManagementConfig finished. Success: ' + result);
        await interaction.reply(`Configuration for ${this.name} saved!`);
    }
    
    public async start(): Promise<void> {
        this.logger.info('ArkManagementBotModule: Start');
        if (this._updateTask == undefined) {
            this.logger.debug('Scheduling Update Task.');
            this._updateTask = schedule('*/5 * * * *', () => this.runUpdate().then());
            this._updateTask.start();
        }
        if (this._chatLogTask == undefined) {
            this.logger.debug('Scheduling ChatLog Task.');
            this._chatLogTask = schedule('*/1 * * * *', () => this.runChatLog().then());
            this._chatLogTask.start();
        }
    }
    
    public async stop(): Promise<void> {
        if (this._updateTask != undefined) {
            this._updateTask.stop();
            this._updateTask = undefined;
        }
        
        if (this._chatLogTask != undefined) {
            this._chatLogTask.stop();
            this._chatLogTask = undefined;
        }
    }

    private async runUpdate(): Promise<void> {
        this.logger.info("runUpdate ...");
        const release = await this._updateMutex.acquire();
        try {
            await this._dataUpdateService.performDataUpdate();
        } catch(e) {
            this.logger.error('runUpdate Exception');
            this.logger.error(e);
        } finally {
            release();
            this.logger.info("runUpdate ... done.");
        }
    }

    private async runChatLog(): Promise<void> {
        this.logger.info("runChatLog ...");
        const release = await this._chatLogMutex.acquire();
        try {
            await this._dataUpdateService.performChatLogUpdate();
        } catch(e) {
            this.logger.error('runChatLog Exception');
            this.logger.error(e);
        } finally {
            release();
            this.logger.info("runChatLog ... done.");
        }
    }
}