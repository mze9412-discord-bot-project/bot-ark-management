// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Ark-Management <https://gitlab.com/mze9412-discord-bot-project/bot-ark-management>.
//
// Bot-Ark-Management is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Ark-Management is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Ark-Management. If not, see <http://www.gnu.org/licenses/>.

import "reflect-metadata";
import { BotLogger, BotRunner, BotRunnerConfig, ConfigService, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { myContainer } from "./inversify.config";
import { DatabaseConfigMongoDb } from "@mze9412-discord-bot-project/bot-database-interface";

const log = myContainer.get<BotLogger>(TYPES_BOTRUNNER.BotLogger).logger;

// register global callbacks
process.once("uncaughtException", err => {
    log.error(err);
});

// register global callbacks
process.once("uncaughtException", err => {
    log.error(err);
});

// log working dir
log.debug(`Working Directory: ${process.cwd()}`);

// get config service and add config for botrunner
const configService = myContainer.get<ConfigService>(TYPES_BOTRUNNER.ConfigService);
configService.addConfig('botRunner', new BotRunnerConfig('owner dicord id', 'bot login token', 'invite URL for bot', true));  // replace with your own config
configService.addConfig('mongoDB', new DatabaseConfigMongoDb('dbhost', 'bot-test', 'password', 'bot-test', 'bot-test'));      // replace with your own config

// get bot
const bot = myContainer.get<BotRunner>(TYPES_BOTRUNNER.BotRunner);
bot.start();

// register to SIGINT and SIGTERM for graceful shutdown
process.once('SIGINT', () => {
    log.info('SIGINT: Stopping');
    bot.stop();
    log.info('SIGINT: DONE');
    process.exit(0);
});
process.once('SIGTERM', () => {
    log.info('SIGTERM: Stopping');
    bot.stop();
    log.info('SIGTERM: DONE');
    process.exit(0);
});