// Copyright (C) 2022 Mathias Zech
// This file is part of Bot-Ark-Management <https://gitlab.com/mze9412-discord-bot-project/bot-ark-management>.
//
// Bot-Ark-Management is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Bot-Ark-Management is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Bot-Ark-Management. If not, see <http://www.gnu.org/licenses/>.

import { Container } from "inversify";
import { BotModule, BotRunner, ConfigService, DefaultModule, DiscordChannelService, DiscordMessageService, DiscordUserService, TYPES_BOTRUNNER } from "@mze9412-discord-bot-project/bot-runner";
import { ArkManagementBotModule } from "../arkManagementBotModule";
import { RconService } from "../services/rcon.service";
import { TYPES_ARKMANAGEMENT } from "../TYPES";
import { ArkServerDatabaseProvider } from "../database/arkServerDatabaseProvider";
import { ServerQueryService } from "../services/serverQuery.service";
import { DataUpdateService } from "../services/dataUpdate.service";
import { ArkServerManagementConfigDatabaseProvider } from "../database/arkServerManagementConfigDatabaseProvider";
import { DiscordSlugDatabaseProvider, DiscordSlugService, TYPES_BOTSHARED } from "@mze9412-discord-bot-project/bot-shared";

const myContainer = new Container();
myContainer.bind<ConfigService>(TYPES_BOTRUNNER.ConfigService).to(ConfigService).inSingletonScope();
myContainer.bind<BotRunner>(TYPES_BOTRUNNER.BotRunner).to(BotRunner).inSingletonScope();

myContainer.bind<DiscordChannelService>(TYPES_BOTRUNNER.DiscordChannelService).to(DiscordChannelService).inSingletonScope();
myContainer.bind<DiscordMessageService>(TYPES_BOTRUNNER.DiscordMessageService).to(DiscordMessageService).inSingletonScope();
myContainer.bind<DiscordUserService>(TYPES_BOTRUNNER.DiscordUserService).to(DiscordUserService).inSingletonScope();
myContainer.bind<DiscordSlugDatabaseProvider>(TYPES_BOTSHARED.DiscordSlugDatabaseProvider).to(DiscordSlugDatabaseProvider);
myContainer.bind<DiscordSlugService>(TYPES_BOTSHARED.DiscordSlugService).to(DiscordSlugService);

myContainer.bind<ArkServerDatabaseProvider>(TYPES_ARKMANAGEMENT.ArkServerDatabaseProvider).to(ArkServerDatabaseProvider).inSingletonScope();
myContainer.bind<RconService>(TYPES_ARKMANAGEMENT.RconService).to(RconService);
myContainer.bind<ServerQueryService>(TYPES_ARKMANAGEMENT.ServerQueryService).to(ServerQueryService);
myContainer.bind<DataUpdateService>(TYPES_ARKMANAGEMENT.DataUpdateService).to(DataUpdateService);
myContainer.bind<ArkServerManagementConfigDatabaseProvider>(TYPES_ARKMANAGEMENT.ArkServerManagementConfigDatabaseProvider).to(ArkServerManagementConfigDatabaseProvider);

// bot modules
myContainer.bind<BotModule>(TYPES_BOTRUNNER.BotModule).to(DefaultModule).inSingletonScope();
myContainer.bind<ArkManagementBotModule>(TYPES_BOTRUNNER.BotModule).to(ArkManagementBotModule).inSingletonScope();

export { myContainer };